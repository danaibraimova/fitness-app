package com.example.fitnessapp;

/**
 * Created by Dana on 12.11.2017.
 */

public interface StepListener {

    public void step(long timeNs);

}